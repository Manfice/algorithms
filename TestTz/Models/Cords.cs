﻿using System;
namespace TestTz.Models
{
    public class Cords
    {
        public int Row { get; set; }
        public int Column { get; set; }
        public int PathValue { get; set; }

        public void SetData(int row, int column, int pathValue)
        {
            Row = row;
            Column = column;
            PathValue = pathValue;
        }
    }
}
