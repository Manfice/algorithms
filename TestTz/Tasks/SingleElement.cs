﻿using System.Linq;

namespace TestTz.Tasks
{
    public class SingleElement
    {
        private int[] _nums;

        public SingleElement()
        {
            _nums = new[] { 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7, 7, 8, 8 };
        }

        public int GetSingleNumber()
        {
            var i = _nums
                .GroupBy(num => num)
                .Where(g => g.Count() < 2)
                .Select(g => g.Key)
                .FirstOrDefault();

            return i;
        }
    }
}
