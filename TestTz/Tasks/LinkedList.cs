﻿using System;
using System.Collections;
using System.Collections.Generic;
using TestTz.Models;

namespace TestTz.Tasks
{
    public class LinkedList<T> : IEnumerable<T>
    {
        private ListNode<T> _head;
        private ListNode<T> _last;
        private int _count;

        public int Count => _count;
        public bool IsEmpty => _count == 0;

        public void Add(T content)
        {
            ListNode<T> node = new ListNode<T>(content);

            if (_head == null)
            {
                _head = node;
            }
            else
            {
                _last.Next = node;
            }
            _last = node;

            _count++;
        }

        public bool Remove(T data)
        {
            ListNode<T> current = _head;
            ListNode<T> previous = null;

            while (current != null)
            {
                if (current.Data.Equals(data))
                {

                    if (previous != null)
                    {
                        previous.Next = current.Next;
                        if (current.Next == null)
                        {
                            _last = previous;
                        }
                    }
                    else
                    {
                        _head = _head.Next;

                        if (_head == null)
                        {
                            _last = null;
                        }
                    }

                    _count--;
                    return true;
                }
                previous = current;
                current = current.Next;
            }

            return false;
        }

        public void RemoveAt(int index)
        {
            ListNode<T> current = _head;

            int position = 0;
            int positionFromEnd = _count - index;

            while (position <= positionFromEnd)
            {
                if (position == positionFromEnd)
                {
                    Remove(current.Data);
                }

                current = current.Next;

                position++;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            ListNode<T> current = _head;
            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }
    }
}
