﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestTz.Models;

namespace TestTz.Tasks
{
    public class MatrixPath
    {
        private int _rows = 4;
        private int _columns = 4;

        public int[,] Matrix { get; set; }
        public List<Cords> PathValues { get; set; }

        public MatrixPath()
        {
            InitMatrix();
        }

        public MatrixPath(int dim)
        {
            _rows = dim;
            _columns = dim;
            InitMatrix();
        }

        public MatrixPath(int rows, int columns)
        {
            _rows = rows;
            _columns = columns;
            InitMatrix();
        }

        private void InitMatrix()
        {
            Matrix = new int[_rows, _columns];
            PathValues = new List<Cords>();

            var rnd = new Random();

            for (int i = 0; i < _rows; i++)
            {
                for (int j = 0; j < _columns; j++)
                {
                    Matrix[i, j] = rnd.Next(10);
                }
            }

            FindPath();
        }

        private void FindPath()
        {
            Cords position = new Cords
            {
                PathValue = Matrix[0, 0]
            };
            PathValues.Add(position);

            while (CanMove(position))
            {
                position = NextStep(position);
                PathValues.Add(position);
            }

            position.SetData(
                Matrix.GetLength(0) - 1,
                Matrix.GetLength(1) - 1,
                Matrix[Matrix.GetLength(0) - 1, Matrix.GetLength(1) - 1]
                );
            PathValues.Add(position);
        }

        private bool CanMove(Cords position)
        {

            return CanMoveDown(position.Row) || CanMoveRight(position.Column);
        }

        private bool CanMoveDown(int row)
        {
            return Matrix.GetLength(0) - 1 > row;
        }

        private bool CanMoveRight(int column)
        {
            return Matrix.GetLength(1) - 1 > column;
        }

        private Cords NextStep(Cords curentPosition)
        {
            var nextPosition = new Cords();

            int matrixValue = int.MaxValue;

            if (CanMoveDown(curentPosition.Row))
            {
                if (matrixValue > Matrix[curentPosition.Row + 1, curentPosition.Column])
                {
                    matrixValue = Matrix[curentPosition.Row + 1, curentPosition.Column];
                    nextPosition.SetData(curentPosition.Row + 1, curentPosition.Column, matrixValue);
                }
            }

            if (CanMoveRight(curentPosition.Column))
            {
                if (matrixValue > Matrix[curentPosition.Row, curentPosition.Column + 1])
                {
                    nextPosition.SetData(
                        curentPosition.Row,
                        curentPosition.Column + 1,
                        Matrix[curentPosition.Row, curentPosition.Column + 1]
                        );
                }
            }

            return nextPosition;
        }

        private void PrintGreen(int row, int column)
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.Write($" {Matrix[row, column]} ");
        }

        private void PrintDefault(int row, int column)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write($" {Matrix[row, column]} ");
        }

        public void PrintResult()
        {
            for (int i = 0; i < Matrix.GetLength(0); i++)
            {
                for (int j = 0; j < Matrix.GetLength(1); j++)
                {
                    if (PathValues.FirstOrDefault(cord => cord.Row == i && cord.Column == j) != null)
                    {
                        PrintGreen(i, j);
                    }
                    else
                    {
                        PrintDefault(i, j);
                    }
                }
                Console.WriteLine();
            }

            Console.WriteLine($"Сумма пути = {PathValues.Sum(v => v.PathValue)}");
        }
    }
}
