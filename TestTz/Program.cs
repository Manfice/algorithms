﻿using System;
using TestTz.Tasks;

namespace TestTz
{
    class Program
    {
        static void Main(string[] args)
        {
            Task1();
            Console.WriteLine("------------------------------------");

            Task2();
            Console.WriteLine("------------------------------------");

            Task3();

            Console.ReadLine();
        }

        static void Task1()
        {
            var sn = new SingleElement();
            Console.WriteLine($"Single element in array: {sn.GetSingleNumber()}");
        }

        static void Task2()
        {

            var list = new LinkedList<int>
            {
                0,
                1,
                2,
                3,
                4,
                5
            };

            foreach (var item in list)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine($"Укажите индекс с конца списка. Не меньше 0 и не больше {list.Count}");
            var indexFromEnd = Console.ReadLine();

            list.RemoveAt(int.Parse(indexFromEnd));

            foreach (var item in list)
            {
                Console.WriteLine(item);
            }

        }

        static void Task3()
        {
            var matrixPath = new MatrixPath();
            matrixPath.PrintResult();
        }
    }
}
